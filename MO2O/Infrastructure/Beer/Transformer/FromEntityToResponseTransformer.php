<?php

namespace App\MO2O\Infrastructure\Beer\Transformer;

use App\MO2O\Domain\Beer\Entity\Beer;

class FromEntityToResponseTransformer
{
    /**
     * @param Beer $beer
     * @return array
     */
    public function transform(Beer $beer): array
    {
        return [
            'id' => $beer->id(),
            'name' => $beer->name(),
            'description' => $beer->description(),
            'image' => $beer->imageUrl(),
            'slogan' => $beer->tagLine(),
            'brewed' => $beer->firstBrewed()->format('Y'),
        ];
    }
}
