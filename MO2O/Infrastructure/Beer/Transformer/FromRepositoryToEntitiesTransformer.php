<?php

namespace App\MO2O\Infrastructure\Beer\Transformer;

use App\MO2O\Domain\Beer\Entity\Beer;
use Carbon\Carbon;

class FromRepositoryToEntitiesTransformer
{
    /**
     * @param array $beers
     * @return array|Beer[]
     */
    public function transform(array $beers): array
    {
        return array_map(
            function (array $beer) {
                return new Beer(
                    $beer['id'],
                    $beer['name'],
                    $beer['description'],
                    $this->buildImageUrl($beer['image_url']),
                    $beer['tagline'],
                    $this->buildDate($beer['first_brewed']),
                );
            }
            , $beers
        );
    }

    /**
     * @param string|null $first_brewed
     * @return Carbon|null
     */
    private function buildDate(?string $first_brewed): ?Carbon
    {
        if (!is_null($first_brewed) && strlen($first_brewed) === 4) {
            return Carbon::createFromFormat('Y', $first_brewed);
        }
        if (!is_null($first_brewed) && strlen($first_brewed) > 4) {
            return Carbon::createFromFormat('m/Y', $first_brewed);
        }
        return null;
    }

    /**
     * @param string|null $imageUrl
     * @return string
     */
    private function buildImageUrl(?string $imageUrl): string
    {
        return is_null($imageUrl) ? 'about:blank' : $imageUrl;
    }
}
