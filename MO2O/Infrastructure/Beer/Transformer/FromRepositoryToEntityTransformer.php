<?php

namespace App\MO2O\Infrastructure\Beer\Transformer;

use App\MO2O\Domain\Beer\Entity\Beer;
use Carbon\Carbon;

class FromRepositoryToEntityTransformer
{
    /**
     * @param array $beer
     * @return Beer
     */
    public function transform(array $beer): Beer
    {
        return new Beer(
            $beer[0]['id'],
            $beer[0]['name'],
            $beer[0]['description'],
            $this->buildImageUrl($beer[0]['image_url']),
            $beer[0]['tagline'],
            $this->buildDate($beer[0]['first_brewed'])
        );
    }

    /**
     * @param string|null $first_brewed
     * @return Carbon|null
     */
    private function buildDate(?string $first_brewed): ?Carbon
    {
        if (!is_null($first_brewed) && strlen($first_brewed) === 4) {
            return Carbon::createFromFormat('Y', $first_brewed);
        }
        if (!is_null($first_brewed) && strlen($first_brewed) > 4) {
            return Carbon::createFromFormat('m/Y', $first_brewed);
        }
        return null;
    }

    /**
     * @param string|null $imageUrl
     * @return string
     */
    private function buildImageUrl(?string $imageUrl): string
    {
        return is_null($imageUrl) ? 'about:blank' : $imageUrl;
    }
}
