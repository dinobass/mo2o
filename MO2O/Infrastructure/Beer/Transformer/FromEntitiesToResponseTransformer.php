<?php

namespace App\MO2O\Infrastructure\Beer\Transformer;

use App\MO2O\Domain\Beer\Entity\Beer;

class FromEntitiesToResponseTransformer
{
    /**
     * @param array|Beer[] $beers
     * @return array
     */
    public function transform(array $beers): array
    {
        return array_map(
            function (Beer $beer) {
                return [
                    'id' => $beer->id(),
                    'name' => $beer->name(),
                    'description' => $beer->description()
                ];
            },
            $beers
        );
    }
}
