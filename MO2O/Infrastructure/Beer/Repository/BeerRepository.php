<?php

namespace App\MO2O\Infrastructure\Beer\Repository;

use App\MO2O\Domain\Beer\Entity\Beer;
use App\MO2O\Domain\Beer\Repository\BeerRepositoryInterface;
use App\MO2O\Domain\Beer\Repository\Criteria;
use App\MO2O\Infrastructure\Beer\Exception\NotFoundOnRepositoryException;
use App\MO2O\Infrastructure\Beer\Transformer\FromRepositoryToEntitiesTransformer;
use App\MO2O\Infrastructure\Beer\Transformer\FromRepositoryToEntityTransformer;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BeerRepository implements BeerRepositoryInterface
{
    private const BEERS_ROUTE = '/beers?food=%s';
    private const BEER_ROUTE = '/beers/%d';

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var FromRepositoryToEntitiesTransformer
     */
    private $fromRepositoryToEntitiesTransformer;

    /**
     * @var FromRepositoryToEntityTransformer
     */
    private $fromRepositoryToEntityTransformer;

    /**
     * BeerRepository constructor.
     * @param string $endpoint
     * @param FromRepositoryToEntitiesTransformer $fromRepositoryToEntitiesTransformer
     * @param FromRepositoryToEntityTransformer $fromRepositoryToEntityTransformer
     */
    public function __construct(
        string $endpoint,
        FromRepositoryToEntitiesTransformer $fromRepositoryToEntitiesTransformer,
        FromRepositoryToEntityTransformer $fromRepositoryToEntityTransformer
    ) {
        $this->httpClient = HttpClient::create();
        $this->endpoint = $endpoint;
        $this->fromRepositoryToEntitiesTransformer = $fromRepositoryToEntitiesTransformer;
        $this->fromRepositoryToEntityTransformer = $fromRepositoryToEntityTransformer;
    }

    /**
     * @param Criteria $criteria
     * @return array|Beer[]
     * @throws NotFoundOnRepositoryException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Exception
     */
    public function search(Criteria $criteria): array
    {
        $url = $this->endpoint . sprintf(self::BEERS_ROUTE, $criteria->food());
        $response = $this->httpClient->request('GET', $url);
        $this->guardAgainstBadResponse($response);
        $content = json_decode($response->getContent(), true);
        return $this->fromRepositoryToEntitiesTransformer->transform($content);
    }

    /**
     * @param string $id
     * @return Beer
     * @throws NotFoundOnRepositoryException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Exception
     */
    public function get(string $id): Beer
    {
        $url = $this->endpoint . sprintf(self::BEER_ROUTE, $id);
        $response = $this->httpClient->request('GET', $url);
        $this->guardAgainstBadResponse($response);
        $content = json_decode($response->getContent(), true);
        return $this->fromRepositoryToEntityTransformer->transform($content);
    }

    /**
     * @param $response
     * @throws NotFoundOnRepositoryException
     */
    private function guardAgainstBadResponse($response)
    {
        if (404 === $response->getStatusCode()) {
            throw new NotFoundOnRepositoryException();
        }
        if (200 !== $response->getStatusCode()) {
            throw new \Exception($response->getStatusCode());
        }
    }
}
