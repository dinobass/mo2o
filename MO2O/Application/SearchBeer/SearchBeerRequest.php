<?php

namespace App\MO2O\Application\SearchBeer;

class SearchBeerRequest
{
    /**
     * @var string
     */
    private $food;

    /**
     * SearchBeerRequest constructor.
     * @param string $food
     */
    public function __construct(string $food)
    {
        $this->food = $food;
    }

    /**
     * @return string
     */
    public function food(): string
    {
        return $this->food;
    }
}
