<?php

namespace App\MO2O\Application\SearchBeer;

use App\MO2O\Domain\Beer\Repository\BeerRepositoryInterface;
use App\MO2O\Domain\Beer\Repository\Criteria;
use App\MO2O\Infrastructure\Beer\Transformer\FromEntitiesToResponseTransformer;
use Psr\Log\LoggerInterface;

class SearchBeerUseCase
{
    /**
     * @var BeerRepositoryInterface
     */
    private $repository;

    /**
     * @var FromEntitiesToResponseTransformer
     */
    private $transformer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SearchBeerUseCase constructor.
     * @param BeerRepositoryInterface $repository
     * @param FromEntitiesToResponseTransformer $transformer
     * @param LoggerInterface $logger
     */
    public function __construct(
        BeerRepositoryInterface $repository,
        FromEntitiesToResponseTransformer $transformer,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->logger = $logger;
    }

    /**
     * @param SearchBeerRequest $request
     * @return SearchBeerResponse
     * @throws \Throwable
     */
    public function execute(SearchBeerRequest $request): SearchBeerResponse
    {
        try {
            $beers = $this->repository->search(new Criteria($request->food()));
            return new SearchBeerResponse($this->transformer->transform($beers));
        } catch (\Throwable $error) {
            $this->logger->error($error->getMessage());
            throw $error;
        }
    }
}
