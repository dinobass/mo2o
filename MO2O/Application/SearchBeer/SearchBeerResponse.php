<?php

namespace App\MO2O\Application\SearchBeer;

class SearchBeerResponse
{
    /**
     * @var array
     */
    private $response;

    /**
     * SearchBeerResponse constructor.
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function response(): array
    {
        return $this->response;
    }
}
