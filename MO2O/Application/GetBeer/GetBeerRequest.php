<?php

namespace App\MO2O\Application\GetBeer;

class GetBeerRequest
{
    /**
     * @var int
     */
    private $id;

    /**
     * GetBeerRequest constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }
}
