<?php

namespace App\MO2O\Application\GetBeer;

use App\MO2O\Domain\Beer\Repository\BeerRepositoryInterface;
use App\MO2O\Infrastructure\Beer\Transformer\FromEntityToResponseTransformer;
use Psr\Log\LoggerInterface;

class GetBeerUseCase
{
    /**
     * @var BeerRepositoryInterface
     */
    private $repository;

    /**
     * @var FromEntityToResponseTransformer
     */
    private $transformer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SearchBeerUseCase constructor.
     * @param BeerRepositoryInterface $repository
     * @param FromEntityToResponseTransformer $transformer
     * @param LoggerInterface $logger
     */
    public function __construct(
        BeerRepositoryInterface $repository,
        FromEntityToResponseTransformer $transformer,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->logger = $logger;
    }

    /**
     * @param GetBeerRequest $request
     * @return GetBeerResponse
     * @throws \Throwable
     */
    public function execute(GetBeerRequest $request): GetBeerResponse
    {
        try {
            $beers = $this->repository->get($request->id());
            return new GetBeerResponse($this->transformer->transform($beers));
        } catch (\Throwable $error) {
            $this->logger->error($error->getMessage());
            throw $error;
        }
    }
}
