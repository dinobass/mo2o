<?php

namespace App\MO2O\Domain\Beer\Entity;

use Carbon\Carbon;

class Beer
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $imageUrl;

    /**
     * @var string
     */
    private $tagLine;

    /**
     * @var Carbon|null
     */
    private $firstBrewed;

    /**
     * Beer constructor.
     * @param int $id
     * @param string $name
     * @param string $description
     * @param string $imageUrl
     * @param string $tagLine
     * @param ?Carbon $firstBrewed
     */
    public function __construct(
        int $id,
        string $name,
        string $description,
        string $imageUrl,
        string $tagLine,
        ?Carbon $firstBrewed
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->imageUrl = $imageUrl;
        $this->tagLine = $tagLine;
        $this->firstBrewed = $firstBrewed;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function imageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @return string
     */
    public function tagLine(): string
    {
        return $this->tagLine;
    }

    /**
     * @return Carbon|null
     */
    public function firstBrewed(): ?Carbon
    {
        return $this->firstBrewed;
    }
}
