<?php

namespace App\MO2O\Domain\Beer\Repository;

class Criteria
{
    /**
     * @var string
     */
    private $food;

    /**
     * Criteria constructor.
     * @param string $food
     */
    public function __construct(string $food)
    {
        $this->food = $food;
    }

    /**
     * @return string
     */
    public function food(): string
    {
        return $this->food;
    }
}
