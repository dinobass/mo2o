<?php

namespace App\MO2O\Domain\Beer\Repository;

use App\MO2O\Domain\Beer\Entity\Beer;

interface BeerRepositoryInterface
{
    /**
     * @param Criteria $criteria
     * @return array|Beer[]
     */
    public function search(Criteria $criteria): array;

    /**
     * @param string $id
     * @return Beer
     */
    public function get(string $id): Beer;
}
