# Welcome to my MO2O kata

I'll be glad to discuss whatever point you consider is remarkable, in my opinion this test is fine to show my tech skills 
but is not a good approach as a final development for a stable API, I can make improvements to the project but I 
need more time for that.

Please run composer install once you cloned the repository on your local, and `symfony server:start` to raise up the project.

I created unit test and also I installed codeception to create API test and check everything is working fine from start to end, 
you can run the whole test suite (unit and api) by running next command.

`$ vendor/bin/codecept run` 

You can find my DDD implementation on the `MO2O` folder placed at the root of the project.

You can see my git history to check how I used git flow branching model. 

Is still pending to document the API, I may do it using swagger for example.
