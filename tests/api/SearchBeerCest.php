<?php

namespace App\Tests;

use ApiTester;
use Symfony\Component\HttpFoundation\Response;

class SearchBeerCest
{
    public function _before(ApiTester $I)
    {
    }


    public function getBeersByFoodCriteria(ApiTester $I)
    {
        $I->sendGET('/beers', ['food' => 'chicken']);
        $I->seeResponseCodeIs(Response::HTTP_OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('[{"id":1,"name":"Buzz","description":"A light, crisp and bitter IPA brewed with English and American hops.');
    }
}
