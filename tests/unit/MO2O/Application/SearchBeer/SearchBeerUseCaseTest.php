<?php

namespace App\Tests\MO2O\Application\SearchBeer;

use App\MO2O\Application\SearchBeer\SearchBeerRequest;
use App\MO2O\Application\SearchBeer\SearchBeerUseCase;
use App\MO2O\Domain\Beer\Entity\Beer;
use App\MO2O\Domain\Beer\Repository\BeerRepositoryInterface;
use App\MO2O\Infrastructure\Beer\Transformer\FromEntitiesToResponseTransformer;
use Carbon\Carbon;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class SearchBeerUseCaseTest extends TestCase
{
    /**
     * @var BeerRepositoryInterface|MockObject
     */
    private $repository;

    /**
     * @var FromEntitiesToResponseTransformer
     */
    private $transformer;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var SearchBeerUseCase
     */
    private $useCase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->getMockBuilder(BeerRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->transformer = new FromEntitiesToResponseTransformer();
        $this->logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->useCase = new SearchBeerUseCase(
            $this->repository,
            $this->transformer,
            $this->logger
        );
    }

    public function testUseCaseReturnsDataWithNoIssues()
    {
        // arrange
        $this->repository->expects($this->once())
            ->method('search')
            ->willReturn(
                [
                    new Beer(1,
                        'estrella damm',
                        'Estrella Damm es una cerveza tipo lager elaborada con malta de cebada, arroz y lúpulo.',
                        'estrella.jpg',
                        'best beer ever',
                        Carbon::now()->subYear()
                    ),
                    new Beer(2,
                        'moritz',
                        'MORITZ EPIDOR cerveza rubia nacional extra botella 33 cl.',
                        'moritz.jpg',
                        'cerveza mediterranea',
                        Carbon::now()->subYears(2)
                    ),
                ]
            );

        //act
        $response = $this->useCase->execute(new SearchBeerRequest('patatas bravas'));

        //assert
        $this->assertEquals(
            [
                [
                    'id' => 1,
                    'name' => 'estrella damm',
                    'description' => 'Estrella Damm es una cerveza tipo lager elaborada con malta de cebada, arroz y lúpulo.',
                ],
                [
                    'id' => 2,
                    'name' => 'moritz',
                    'description' => 'MORITZ EPIDOR cerveza rubia nacional extra botella 33 cl.',
                ],
            ],
            $response->response()
        );
    }

    public function testUseCaseFailsOnRepositoryAndReturnsError()
    {
        // arrange
        $this->repository->expects($this->once())
            ->method('search')
            ->willThrowException(new \Exception('my error'));

        $this->expectExceptionMessage('my error');

        $this->logger->expects($this->once())
            ->method('error');

        //act
        $this->useCase->execute(new SearchBeerRequest('patatas bravas'));
    }
}
