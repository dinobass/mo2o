<?php

namespace App\Tests\MO2O\Application\GetBeer;

use App\MO2O\Application\GetBeer\GetBeerRequest;
use App\MO2O\Application\GetBeer\GetBeerUseCase;
use App\MO2O\Domain\Beer\Entity\Beer;
use App\MO2O\Domain\Beer\Repository\BeerRepositoryInterface;
use App\MO2O\Infrastructure\Beer\Transformer\FromEntityToResponseTransformer;
use Carbon\Carbon;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class GetBeerUseCaseTest extends TestCase
{
    /**
     * @var BeerRepositoryInterface|MockObject
     */
    private $repository;

    /**
     * @var FromEntityToResponseTransformer
     */
    private $transformer;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var GetBeerUseCase
     */
    private $useCase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->getMockBuilder(BeerRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->transformer = new FromEntityToResponseTransformer();
        $this->logger = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->useCase = new GetBeerUseCase(
            $this->repository,
            $this->transformer,
            $this->logger
        );
    }

    public function testUseCaseReturnsDataWithNoIssues()
    {
        // arrange
        $this->repository->expects($this->once())
            ->method('get')
            ->willReturn(
                new Beer(1,
                    'estrella damm',
                    'Estrella Damm es una cerveza tipo lager elaborada con malta de cebada, arroz y lúpulo.',
                    'estrella.jpg',
                    'best beer ever',
                    Carbon::now()->subYear()
                )
            );

        //act
        $response = $this->useCase->execute(new GetBeerRequest(1));

        //assert
        $this->assertEquals(
            [
                'id' => 1,
                'name' => 'estrella damm',
                'description' => 'Estrella Damm es una cerveza tipo lager elaborada con malta de cebada, arroz y lúpulo.',
                'image' => 'estrella.jpg',
                'slogan' => 'best beer ever',
                'brewed' => Carbon::now()->subYear()->format('Y')

            ],
            $response->response()
        );
    }

    public function testUseCaseFailsOnRepositoryAndReturnsError()
    {
        // arrange
        $this->repository->expects($this->once())
            ->method('get')
            ->willThrowException(new \Exception('my error'));

        $this->expectExceptionMessage('my error');

        $this->logger->expects($this->once())
            ->method('error');

        //act
        $this->useCase->execute(new GetBeerRequest(1));
    }
}
