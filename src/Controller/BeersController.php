<?php

namespace App\Controller;

use App\MO2O\Application\GetBeer\GetBeerRequest;
use App\MO2O\Application\GetBeer\GetBeerUseCase;
use App\MO2O\Application\SearchBeer\SearchBeerRequest;
use App\MO2O\Application\SearchBeer\SearchBeerUseCase;
use App\MO2O\Infrastructure\Beer\Exception\NotFoundOnRepositoryException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class BeersController extends AbstractController
{
    /**
     * @Route("/beers")
     * @param Request $request
     * @param SearchBeerUseCase $useCase
     * @return JsonResponse
     * @throws \Throwable
     */
    public function index(Request $request, SearchBeerUseCase $useCase)
    {
        try {
            $food = $request->query->get('food');
            $violations = Validation::createValidator()->validate($food, [new NotBlank()]);
            if ($violations->count() > 0) {
                throw new \Exception("food: " . $violations->get(0)->getMessage());
            }

            $response = $useCase->execute(new SearchBeerRequest($food));
            return new JsonResponse($response->response());
        } catch (NotFoundOnRepositoryException  $error) {
            return new JsonResponse(['error' => 'not found'], Response::HTTP_NOT_FOUND);
        } catch (\Throwable  $error) {
            return new JsonResponse(['error' => $error->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/beers/{id}")
     * @param string $id
     * @param GetBeerUseCase $useCase
     * @return JsonResponse
     */
    public function getBeer(string $id, GetBeerUseCase $useCase)
    {
        try {
            $response = $useCase->execute(new GetBeerRequest($id));
            return new JsonResponse($response->response());
        } catch (NotFoundOnRepositoryException  $error) {
            return new JsonResponse(['error' => 'not found'], Response::HTTP_NOT_FOUND);
        } catch (\Throwable  $error) {
            return new JsonResponse(['error' => $error->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
